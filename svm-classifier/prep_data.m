%% Given EEG samples, tapping frequencies (period), and times of events
%% (taps), chop data into epochs and assign labels of 1 or -1

function [data,labels] = prep_data(data,events,p)

