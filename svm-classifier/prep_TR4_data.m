function [data2,labels2] = prep_TR4_data(p)
    load(strcat('Data/TR4/simV',num2str(p)))
    setC = vtot(:,31:162+30);
    load(strcat('Data/TR4/test',num2str(p)))
    labC = tl;

    labels = labC > 0;

    % trim data to be divisible by p
    %data = data(:,)
    newend = length(setC) - mod(length(setC),p);
    setC = setC(:,1:newend);

    % reshape data into epochs 
    data2 = [];
    labels2 = [];
    numsegs = length(setC)/p;
    for i = 1:numsegs
        if length(labels(1,:)) >= i % only add if not in first 25 of model time
            data2 = [data2 ; setC(:,(i-1)*p+1:i*p)];
            labels2 = [labels2 ; labels(:,i)];
        end
    end