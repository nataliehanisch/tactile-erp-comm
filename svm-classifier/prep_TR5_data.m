function [data2,labels2] = prep_TR5_data(p)
    load(strcat('Data/TR5/simV',num2str(p)))
    setC = vtot(:,31:200);
    load(strcat('Data/TR5/test',num2str(p)))
    labC = tl;

    labels = labC > 0;

    % trim data to be divisible by p
    %data = data(:,)
    newend = length(setC(1,:)) - mod(length(setC(1,:)),p);
    setC = setC(:,1:newend);

    % reshape data into epochs 
    data2 = [];
    labels2 = [];
    numsegs = length(setC)/p;
    for i = 1:numsegs
        if length(labels(1,:)) >= i % only add if not in first 25 of model time
            data2 = [data2 ; setC(:,(i-1)*p+1:i*p)];
            labels2 = [labels2 ; labels(:,i)];
        end
    end