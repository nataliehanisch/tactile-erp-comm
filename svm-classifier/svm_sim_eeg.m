function [accuracy,confidence,testL,scores] = svm_sim_eeg(data_unsorted,labels_unsorted,tss,dispout,showplot)

if nargin < 3
    dispout = 0;
    showplot = 0;
    ratio = 0.05; % percent of data to use for training
    tss = floor(length(data_unsorted(:,1))*ratio);
    if tss < 1
       tss = 1;
    end
end

%% divide into training and testing data
ordering = randperm(length(data_unsorted(:,1)));
data_unsorted = data_unsorted(ordering,:);
labels = labels_unsorted(ordering,:);


qua = tss; % training set size
train = data_unsorted(1:qua,:);
test  = data_unsorted(qua+1:end,:);

% provide labels 
trainL = labels(1:qua,:); 
testL  = labels(qua+1:end,:); 

%% train SVM
SVMModel = fitcsvm(train,trainL,'KernelFunction','rbf','Standardize',true);

%% test/classify
[label,scores] = predict(SVMModel,test);

%% plot
%scatter(1:length(label),testL==label);
accuracy = sum(testL==label)/length(label);
confidence = mean(abs(scores(:,1)))/2;
%disp(strcat('Percent Correct: ',num2str(accuracy)));

if dispout
    disp(strcat('Training set size: ',num2str(length(train(:,1)))))
    disp(strcat('Test set size: ',num2str(length(test(:,1)))))
    disp(strcat('Average confidence: ',num2str(confidence)))
    disp(strcat('Score: ',num2str(accuracy)))
end
    
if showplot
    %% sort
    [testL_sorted,I] = sort(testL,'descend');
    scores_sorted = scores(I,:);

    hold on
    plot(scores_sorted(:,2))
    %scatter(1:(midpt-qua)*2,label)
    scatter(1:length(testL_sorted),testL_sorted)
    legend('Score','True Label')
    %plot(ones(1,3)*(midpt-qua),-0.01:0.01:0.01)
end