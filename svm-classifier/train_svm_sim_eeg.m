function [SVMModel] = svm_sim_eeg(training_data,labels)

% unless other data processing is needed this prob doesn't need it's own
% file

%% train SVM
SVMModel = fitcsvm(training_data,labels,'KernelFunction','rbf','Standardize',true);
