function [data2,labels2] = prep_TR1_TR2_data(p)
    
    load(strcat('Data/TR1/simV',num2str(p)))
    setA = vtot;
    load(strcat('Data/TR2/simV',num2str(p)))
    setB = vtot;

    load(strcat('Data/TR1/test000',num2str(p)))
    labA = tl;
    load(strcat('Data/TR2/test',num2str(p)))
    labB = tl; 
    % end

    data = [setA ; setB];
    events = [labA ; labB];    
    %imagesc(data)

    labels = events > 0;

    % trim data to be divisible by p
    %data = data(:,)
    newend = length(data) - mod(length(data),p);
    data = data(:,1:newend);

    % reshape data into epochs 
    data2 = [];
    labels2 = [];
    numsegs = length(data)/p;
    for i = 1:numsegs
        if (i-1)*p+1 >= 25 && length(labels(1,:)) >= i % only add if not in first 25 of model time
            data2 = [data2 ; data(:,(i-1)*p+1:i*p)];
            labels2 = [labels2 ; labels(:,i)];
        end
    end