res = zeros(12,2);
num_2_avg = 1000;
tic
for p = 1:12
    disp(p)
    [d1,l1] = prep_TR1_TR2_data(p);
    [d2,l2] = prep_TR4_data(p);
    data = [d1;d2];
    labels = [l1;l2];
    % add TR3 data
    tmp_res = 0;
    for i = 1:num_2_avg
        if ~isempty(data) && ~isempty(labels)
            [a,c] = svm_sim_eeg(data,labels,500,0,0);
            tmp_res = tmp_res + a;
        end 
    end
    avg_for_p = tmp_res/num_2_avg;
    res(p,:) = [avg_for_p,abs(c)];
    toc
end
%y = (1:12)*100/12;
%z = 1000./y;
%plot(y,res(:,1))
% xscale = ((1:length(res))*100/12);
% figure(3)
% plot(xscale,res(:,1)) %accuracy
% figure(4)
% plot(xscale,smooth(res(:,1)))
% 
% xscale = 1000./((1:length(res))*100/12);
% yscale = res(:,1);
%plot(xscale,yscale)
%save('svm_res','res')

xscale = 1000./((1:length(res))*100/12);
yscale = res(:,1);
figure(1)
plot(xscale,smooth(yscale))
hold on
figure(2)
plot(xscale,smooth(res(:,2)))
hold on
figure(3)
plot(xscale,smooth(res(:,1).*res(:,2)))
hold on

% figure
% plot(res(:,2)) % confidence
% figure
% plot(res(:,1).*res(:,2))

%xlabel('frequency')
%ylabel('accuracy')