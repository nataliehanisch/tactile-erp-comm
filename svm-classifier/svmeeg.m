%% load data
load('Data/fullSignal.mat')
load('Data/fullSignalEvents.mat')

%% divide into training and testing data
taps = []; %zeros(150*9,100);
notaps = [];
front = zeros(9,100);
back = front;
ev = [fullSignalEvents.latency];

for i = 1:2:75
    front = fullSignal(:,ev(i):ev(i)+99);
    back = fullSignal(:,ev(i)-100:ev(i)-1);
    taps = [taps ; front];
    notaps = [notaps ; back];
    %data = [front ; data ; back];
end

data = [taps; notaps];
labels = [ones(length(taps),1) ; ones(length(notaps),1)*-1];
taps = [];
notaps = [];

for i = 2:2:75
    front = fullSignal(:,ev(i):ev(i)+99);
    back = fullSignal(:,ev(i)-100:ev(i)-1);
    taps = [taps ; front];
    notaps = [notaps ; back];
    %data = [front ; data ; back];
end

data = [data; taps; notaps];
labels = [labels; ones(length(taps),1) ; ones(length(notaps),1)*-1];

ratio = 0.95; % percent of data to use for training
midpt = floor(length(data)/2);
qua = floor(midpt*ratio);
train = [data(1:qua,:) ; data(midpt*2+1-qua:end,:)];
test  = [data(qua+1:midpt,:) ; data(midpt+1:midpt*2+1-qua-1,:)];

trainL = [labels(1:qua,:) ; labels(midpt*2+1-qua:end,:)]; %[ones(qua,1);-1*ones(qua,1)];
testL  = [labels(qua+1:midpt,:) ; labels(midpt+1:midpt*2+1-qua-1,:)]; %[ones(midpt-qua,1);-1*ones(midpt-qua,1)];

%% train SVM
SVMModel = fitcsvm(train,trainL,'KernelFunction','rbf','Standardize',true);

%% test/classify
[label,scores2] = predict(SVMModel,test);

%% plot
hold on
plot(scores2(:,2))
scatter(1:(midpt-qua)*2,testL)
legend('Score','True Label')