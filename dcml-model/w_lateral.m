function [wl] = w_lateral(q)
    % This function returns the lateral connections between two neurons.

    %q = toral_dist(getxy(x,fw),getxy(y,fw),fw);

    ke = 3.65; % k_e
    se = 0.10; % sigma_e
    ki = 2.40; % k_i
    si = 1.00; % sigma_i

    %wl = ke*exp((-q^2)/(2*se^2)) - ki*exp((-q^2)/(2*si^2));
    wl = ke*exp((-q.^2)/(2*se^2)) - ki*exp((-q.^2)/(2*si^2));