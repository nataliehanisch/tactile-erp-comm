function [vtot] = generate_V(dir,taplist_fn,p)
    tic
    toc
    fn = strcat(taplist_fn,num2str(p));
    load(strcat(dir,fn)) % load list of taps

    dt = 0.01;% 100 milliseconds    0.001;
    %seconds = 0.2;
    time_count = 162;%seconds/dt;3450
    neuron_count = 1024;
    tau = 1;
    c = 1;

    receptor_count = 256;

    seed = 1;
    rng('default'); rng(seed);

    %% load weights
    load(strcat(dir,'weights_from_Detorakis_code.mat'))
    wf2 = wf(:,:,ones(1,time_count));

    trials = 1;
    stimuli = repmat(2.5,2,trials);      %[1,1];

    thresh = 50;

    u = zeros(neuron_count,time_count);%-0.00007; % resting potential
    a = [1,1,1,1,1,1,2,2,2,1,1,2,3,2,1,1,2,2,2,1,1,1,1,1,1];

    q = getq(neuron_count)*0.5;
    x = 1;
    z = 1; % filler
    t = 0;

    du2 = [];

    vtot = [];

    for list = 1:10

        disp(strcat(num2str(list),'/10'))
        taplist = tl(list,:);
        
        for trial = 1:trials

            %disp(strcat(strcat(num2str(trial),'/'),num2str(trials)));

            for t = 1:time_count
                %disp(strcat('DCML: ',num2str(t),'/',num2str(time_count)))
                du = ones(neuron_count,1);

                if any(t==taplist)     %0 == mod(t,10) %50150
                    s = receptor_response(stimuli(:,1)',receptor_count); %always same point
                    %disp(t);
                else
                    s = zeros(receptor_count,1);
                end


                tqc = get_tqc(t,q,c,neuron_count,u);

                % Calculate firing rate
                f = firing_rate(tqc);

                % integrate lateral connections
                lat_integral = sum(w_lateral(q).*f)';

                I = neuron_input(x,z,t,wf2,s);

                % calculate change in membrane potential
                du = dt*(-u(:,t) + lat_integral + I)/tau;

                u(:,t+1) = u(:,t) + du; % Update membrane potential

            end
        end


        xbar = -1.8438;
        ybar = 1.6685; %-2.6647;
        zbar = -4.3332;

        % cortical area = 32x32, 1024 neurons
        cw = 32;
        r = [];
        for i = 1:cw
            r = [r [ones(1,cw)*i; 1:cw ; ones(1,cw)*40]];
        end
        r(1:2,:) = (r(1:2,:)-16); %center
        r = r/(10^6); % compress
        r(1,:) = r(1,:) + xbar; % shift x
        r(2,:) = r(2,:) + ybar; % shift y
        r(3,:) = r(3,:) + zbar; % shift z

        % create eeg surface, 16 electrodes in 4x4 area
        ec = 3;

        eeg_srf = [1,1,1;1,2,0;1,2,0]; % 3 chans for speed

        V = zeros(length(eeg_srf),time_count);

        toc
        for t = 1:time_count
            for e = 1:length(eeg_srf)

                r0 = eeg_srf(:,e);
                V(e,t) = eeg(r0,r,u,t);

            end
            %disp(strcat('EEG: ',num2str(t),'/',num2str(time_count)));
        end
        
        vtot = [vtot; V(2,:)];
        
    end

    save(strcat('simV',num2str(p)),'vtot')

    toc