time_count = length(u(1,:));

xbar = -1.8438;
ybar = 1.6685; %-2.6647;
zbar = -4.3332;

% cortical area = 32x32, 1024 neurons
cw = 32;
r = [];

for i = 1:cw
    r = [r [ones(1,cw)*i; 1:cw ; ones(1,cw)*40]];
end

r(1:2,:) = (r(1:2,:)-16); %center
r = r/(10^6); % compress
r(1,:) = r(1,:) + xbar; % shift x
r(2,:) = r(2,:) + ybar; % shift y
r(3,:) = r(3,:) + zbar; % shift z

% create eeg surface, 16 electrodes in 4x4 area
eeg_srf = [1,1,1,16,16,16,32,32,32;1,16,32,1,16,32,1,16,32; ones(1,9)*2];

V = zeros(length(eeg_srf),time_count);

for t = 1:time_count
    tic
    for e = 1:length(eeg_srf)
        
        r0 = eeg_srf(:,e);
        V(e,t) = eeg(r0,r,u,t);
        
    end
    disp(strcat(num2str(t),'/',num2str(time_count)));
    toc
end

