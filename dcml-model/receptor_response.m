function [ s ] = receptor_response(z,n)
    % Calculates a vector or responses s for each receptor
    % due to a stimulus at location z

    seed = 1;
    rng('default'); rng(seed);

    %n = 256; % num of receptors
    %r = rand(n,2)*5; % create receptors
    load('receptor-locations-detorakis.mat');
    r = R;
    s = zeros(n,1); % storage for receptor responses

    [Y,I]=sort(r(:,1));
    r=r(I,:); 

    sigma = 1/100; % -1/2
    % calculate responses
    for i = 1:n
        s(i) = exp((-1*sigma)*norm(z-r(i,:)));
    end

    end