taplist = []; % list of time point at which to send tap

dt = 0.01;% 100 milliseconds    0.001;
%seconds = 0.2;
time_count = 140;%seconds/dt;3450
neuron_count = 1024;
tau = 1;
c = 1;

receptor_count = 256;

seed = 1;
rng('default'); rng(seed);

%% load weights
load(strcat(cd,'/weights_from_Detorakis_code.mat'));
wf2 = wf(:,:,ones(1,time_count));

trials = 1;
stimuli = repmat(2.5,2,trials);      %[1,1];

thresh = 50;

u = zeros(neuron_count,time_count);%-0.00007; % resting potential
a = [1,1,1,1,1,1,2,2,2,1,1,2,3,2,1,1,2,2,2,1,1,1,1,1,1];

q = getq(neuron_count)*0.5;
x = 1;
z = 1; % filler
t = 0;

du2 = [];

for trial = 1:trials
    
    disp(strcat(strcat(num2str(trial),'/'),num2str(trials)));
    
    for t = 1:time_count
        disp(strcat(num2str(t*100/time_count),'%'))
        du = ones(neuron_count,1);
        
        if any(t==taplist)     %0 == mod(t,10) %50150
            s = receptor_response(stimuli(:,1)',receptor_count); %always same point
            disp(t)
        else
            s = zeros(receptor_count,1);
        end
        
        
        tqc = get_tqc(t,q,c,neuron_count,u);

        % Calculate firing rate
        f = firing_rate(tqc);

        % integrate lateral connections
        lat_integral = sum(w_lateral(q).*f)';
% 
%         if t == 1
        I = neuron_input(x,z,t,wf2,s);
%         else
%             I = 0;
%         end
        
        % calculate change in membrane potential
        du = dt*(-u(:,t) + lat_integral + I)/tau;
        
        u(:,t+1) = u(:,t) + du; % Update membrane potential
        
        du2 = [du2 ; du' ];
        
        %imagesc(reshape(u(:,t),32,32));
        %colorbar;
        %figure; hold on;
        %plot(du*dt);
        %drawnow;

%         imagesc(reshape(u(:,t),32,32))
%         drawnow
%         pause(0.1)
        
    end
end