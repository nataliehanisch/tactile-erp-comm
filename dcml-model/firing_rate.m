function [x] = firing_rate(x)

    fr = 0;% -0.000055; % neuron firing rate value

    indices= x<fr; %find the elements of X, which are below fr
    x(indices)=0; %set all the elements of x which are <fr to zero.
