%% Generate tapping lists to test detection precision
% 1000 tests = 10 tests for each of 100 frequencies

seed = 1;
s = rng;
rng(seed);

tl = [];
lower_lim = 30; % time it takes for model to stabilize
upper_lim = 162; % last time point before model destabilizes
qty = 1000;

for p = 1:12 % period
    tl = repmat(lower_lim:p:upper_lim,qty,1);
    
    %mp = (rand(length(tl(1,:)),qty)-0.5)>0;
    mp = zeros(length(tl(1,:)));
    cc = floor(1 + (floor(132/p)-1).*rand(1000,1));
    for c = 1:1000
        mp(c,cc(c)) = 1;
    end
    
    tl = tl.*mp;
    
    % save var
    save(['test',num2str(p)],'tl')
    
end

