function [ I ] = neuron_input( ~,~,t,wf,s)
    % Produces input I at time t for neuron x due to stimulus z
    % where w is the feed forward weight

    %%% FINGER PAD %%%
    n = length(s);

    %%% DCML %%%
    neuron_count = length(wf(1,:,1));

    % compute neuron inputs I
    total = sum(abs(s(:,ones(1,neuron_count)) - wf(:,:,t))); 

    I = 1 - (1/n)*total; 
    I = I';
    end

