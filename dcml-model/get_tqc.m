function [tqc] = get_tqc(t,q,c,neuron_count,u)

    tqc = t - q/c;
    neg = tqc>=0;
    tqc = round(tqc.*neg);
    
    for x = 1:neuron_count
        for y = 1:neuron_count
            if tqc(x,y) == 0
                tqc(x,y) = 0;
            else
                tqc(x,y) = u(x,tqc(x,y));
            end
        end
    end

end