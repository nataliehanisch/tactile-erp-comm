function [ V ] = eeg( r0,r,u,t )

    si = 1; %0.025; % 0.025 giant squid
    se = 1; %0.05; % 0.05
    n = length(r); % number of neurons

    % solid angle function
    % n, v number and coordinated of neurons in cortex
    % p location of current electrode
    omega = polygon_solid_angle_3d(n,r,r0);

    V = sum(u(:,t)*omega);

    V = V*si/(4*pi*se);

    end