function [ d ] = toral_dist( p1,p2,m )
    % Calculates the distance between two points on a toral shape 
    % of equal height and width m.

    xa = max(p1(1),p2(1));
    xb = min(p1(1),p2(1));
    ya = max(p1(2),p2(2));
    yb = min(p1(2),p2(2));

    % min x distance
    x_dist = min(xa - xb,(m - xa) + (xb - 0));

    % min y distance
    y_dist = min(ya - yb,(m - ya) + (yb - 0));

    d = sqrt(x_dist^2 + y_dist^2);
    end

