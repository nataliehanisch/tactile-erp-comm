% count = 1;
% taplist = zeros(75,1);
% 
% for i = 1:34500
%     if 0 == mod(i,1150) && ~(0 == mod(i,1150*2))
%          taplist(count) = i;
%          taplist(count+1) = i + 230;
%          taplist(count+2) = i + 230*2;
%          taplist(count+3) = i + 230*3;
%          taplist(count+4) = i + 230*4;
%          count = count + 5; 
%     end
% end
% 
% d = fdesign.lowpass('Fp,Fst,Ap,Ast',1,3,0.1,10,11);
% Hd = design(d,'equiripple');
% output = filter(Hd,t);
% plot(output)

    hpFilt = designfilt('highpassiir', 'FilterOrder', 8, ...
             'PassbandFrequency', 75e2, 'PassbandRipple', 0.2,...
             'SampleRate', 200e3);
    %fvtool(hpFilt) % visualize filter response

    y = filter(hpFilt,t); % apply filter to your data
    plot(y)