function [q] = getq(neuron_count)

    field_width = sqrt(neuron_count);

    q = zeros(neuron_count);
    for x = 1:neuron_count
        [m1,n1] = ind2sub(sqrt(size(q)),x);
        for y = 1:neuron_count
            [m2,n2] = ind2sub(sqrt(size(q)),y);
            q(x,y) = toral_dist([m1,n1],[m2,n2],field_width);
        end
    end

    end