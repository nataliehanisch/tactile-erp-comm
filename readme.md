# Detecting Artificial Signals Transmitted through the DCML pathway of the Human Nervous System

This code is supplementary material related to my MS Thesis: 

> [Exploring the Telecommunications Properties of the Human Nervous System:   
> Analytical Modeling and Experimental Validation of Information Flow through the Somatosensory System](http://digitalcommons.unl.edu/computerscidiss/132/)

Additionally, parts of this software were used in the related research from this paper:

> [Digital modulation and achievable information rates of thru-body haptic communications](https://www.spiedigitallibrary.org/conference-proceedings-of-spie/10206/1020603/Digital-modulation-and-achievable-information-rates-of-thru-body-haptic/10.1117/12.2262842.short?SSO=1)

In brief, these projects involve the collection of *in-vivo* EEG data as well as the generation of artificial EEG data for the purposes of developing a signal processing routine which can successfully capture the signature of artificial information transmitted through the nervous system. 

This is a highly interdisciplinary project. Some general topics which may be useful for understanding the applications of this software include:

- The DCML Pathway ([see this video](https://www.youtube.com/watch?v=70Kg4uPem4U))
- Support vector machines
- Digital signal processing
- EEG signal acquisition and processing
- Telecommunications theory

## Requirements

This software was initially developed on MATLAB r2015b, however it was most recently run in full on r2017a with no issues. 

## Acknowledgments

* [Massimiliano Pierobon ](https://mbite.unl.edu/massimiliano-pierobon)
* [The MBiTe Lab](https://mbite.unl.edu/)

